import pathlib
import argparse
import os
from PIL import Image, ImageDraw, ImageFont

class ImageMarkMaker():
    main_path = None   # path where stored source-images folder
    save_path = None   # path where user wants to save markered images
    default_files_path = 'source-images'
    system_files_start = ('.', '_')    # list of symbols with which system file names can be start

    def mkdir(self):
        """
            if user wants to save to new folder we need to create it
        """
        try:
            os.mkdir(f'{self.main_path}/{self.save_path}')
        except FileExistsError:
            pass
        except OSError as error:
            print (f'Не удалось создать папку {self.main_path}/{self.save_path}.')

    def mark_images(self):
        """
            marking images
        """

        # check if we need to create new folder
        if self.save_path != self.default_files_path:
                self.mkdir()
        
        # geting all images from folder
        full_path = f'{self.main_path}/{self.default_files_path}'
        files = {
            f'{file}': f'{full_path}/{file}'
            for file in os.listdir(full_path) if
            os.path.isfile(os.path.join(full_path, file)) and not file.startswith(self.system_files_start)
        }

        # markink images
        for key, value in files.items():
            img = Image.open(value)
            width, height = img.size
            draw_img = ImageDraw.Draw(img)
            my_font = ImageFont.truetype(f'{pathlib.Path(__file__).parent.absolute()}/Arial-BoldMT.ttf', 24)
            author_name = pathlib.Path(value).stem
            draw_img.text((width - 15, height - 15), f"{author_name}", font=my_font, fill=(255, 0, 0), anchor='rs')
            img.save(f'{self.main_path}/{self.save_path}/{key}')

    def get_files_path(self):
        """
            check can we find source-images folder
        """

        # check if source-images folder in place that script store
        if os.path.exists(f'{pathlib.Path(__file__).parent.absolute()}/{self.default_files_path}'):
            self.main_path = pathlib.Path(__file__).parent.absolute()
            return True

        # check if source-images folder in place that we run script
        elif os.path.exists(f'{pathlib.Path().absolute()}/{self.default_files_path}'):
            self.main_path = pathlib.Path().absolute()
            return True
        print(
            'Ни в месте где хранится данный скрипт, ни в месте от куда этот скрипт запущен - '
            'не найдена папка `source-images`!'
        )

        # asking user to input where source-images
        while True:
            user_input_path = input('Пожалуйста укажите путь до папки `source-images`: ')
            if os.path.exists(f'{user_input_path}/{self.default_files_path}'):
                self.main_path = user_input_path
                return True
            print(f'В директории {user_input_path} не найдена папка `source-images`!')


# reading args from terminal
parser = argparse.ArgumentParser()
parser.add_argument("-s", "--save", help="set path to store markered images")
args = parser.parse_args()


if __name__ == '__main__':
    procedure = ImageMarkMaker()
    procedure.get_files_path()
    if args.save:
        procedure.save_path = args.save
    else:
        procedure.save_path = procedure.default_files_path
    procedure.mark_images()
